import java.io.*;

/**
 * Класс позволяет отобрать из 6-значных чисел "счастливые билеты" и записать их в файл lucky.txt
 *
 * @author Андрюшин, 16ИТ18к
 */
public class FindLuckyTicket {
    public static void main(String[] args) {
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream("src\\int6data.dat"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\lucky.txt"))) {
            int number;
            while (inputStream.available() > 0) {
                if (isLucky(number = inputStream.readInt())) {
                    bufferedWriter.write(String.valueOf(number) + "\n");
                }
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод позволяет определить, является ли 6-значное число номером счастливого билета
     *
     * @param number - номер билета
     * @return true, если билет счастливый, false в любом другом случае
     */
    private static boolean isLucky(int number) {
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i < 6; i++) {
            if (i < 3) {
                sum1 += number % 10;
            } else {
                sum2 += number % 10;
            }
            number = number / 10;
        }
        return sum1 == sum2;
    }

    /**
     * Метод, позволяющий определить, является ли число номером счастливого билета
     * для чисел в диапазоне от 1_000 до 999_999
     *
     * @param number - номер билета
     * @return true, если билет счастливый, false в любом другом случае
     */
    private static boolean isLuckyAll(int number) {
        StringBuilder string = new StringBuilder();
        int length = String.valueOf(number).length();
        for (int i = 0; i < 6 - length; i++) {
            string.append("0");
        }
        string.append(String.valueOf(number));
        char[] array = string.toString().toCharArray();
        return array[0] + array[1] + array[2] == array[3] + array[4] + array[5];
    }
}