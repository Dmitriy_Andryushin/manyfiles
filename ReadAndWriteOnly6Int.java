import java.io.*;

/**
 * Класс осуществляет чтение целых чисел из файла, отбор 6-значных чисел и их запись в файл int6data.dat
 *
 * @author Андрюшин, 16ИТ18к
 */
public class ReadAndWriteOnly6Int {
    public static void main(String[] args) {
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream("src\\intdata.dat"));
             DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("src\\int6data.dat"))) {
            int number;
            while (inputStream.available() > 0) {
                number = inputStream.readInt();
                if ((number < 1_000_000) && (number > 99_999)) {
                    outputStream.writeInt(number);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
